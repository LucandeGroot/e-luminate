/*
 * adc.c
 *
 *  Created on: 5 dec. 2017
 *      Author: lucan
 */

#include "adc.h"

#include "bluetooth.h"

#include "em_gpio.h"
#include "em_adc.h"
#include "em_cmu.h"

#include <inttypes.h> // for sprintf with uint32_t
#include <stdio.h> // for sprintf

// can't find it in the header file for some reason
#define NUMBER_OF_SCANS 1

void init_adc_single(){
	ADC_InitSingle_TypeDef sInit = ADC_INITSINGLE_DEFAULT;

	sInit.reference = adcRefVDD; 				// input reference is 3.3V
	sInit.acqTime = adcAcqTime1;				// higher acquisition time (1-256) is better for high impedance circuits
	sInit.posSel = adcPosSelAPORT4XCH5; 		// PD13 see https://www.silabs.com/documents/login/data-sheets/efr32bg1-datasheet.pdf page 141
	sInit.resolution = adcRes12Bit; 			// 6-8-12-Ovs(12-14-16) Bit, select oversampling here
	sInit.leftAdjust = false;					// left adjust the result

	// configure the single conversion mode
	ADC_InitSingle(ADC0, &sInit);

	// disable all other pins on the channel and enable the input
	GPIO_PinModeSet(gpioPortA,1,gpioModeDisabled, 0); 	// PA1
	GPIO_PinModeSet(gpioPortA,3,gpioModeDisabled, 0); 	// PA3
	GPIO_PinModeSet(gpioPortA,5,gpioModeDisabled, 0); 	// PA5
	GPIO_PinModeSet(gpioPortB,13,gpioModeDisabled, 0); 	// PB13
	GPIO_PinModeSet(gpioPortB,11,gpioModeDisabled, 0); 	// PB11
	GPIO_PinModeSet(gpioPortD,11,gpioModeDisabled, 0);	// PD11
	GPIO_PinModeSet(gpioPortD,13,gpioModeInput, 0);		// <<-- INPUT (PD13)
	GPIO_PinModeSet(gpioPortD,15,gpioModeDisabled, 0); 	// PD15
}

void init_adc_scan(){
	ADC_InitScan_TypeDef sInit = ADC_INITSCAN_DEFAULT;

	sInit.reference = adcRefVDD; 			// input reference is 3.3V
	sInit.fifoOverwrite = true;				// overwrite anything still in the fifo buffer
	ADC_InitScan(ADC0, &sInit);

	// add channels too scan
	// max 8 channels per group, 4 groups
	ADC_ScanSingleEndedInputAdd(&sInit, adcScanInputGroup0, adcPosSelAPORT4XCH5);
}

void initADC(){
	/* Enable ADC clock */
	CMU_ClockEnable(cmuClock_ADC0, true);

	/* Base the ADC configuration on the default setup. */
	ADC_Init_TypeDef init = ADC_INIT_DEFAULT;


	init.timebase = ADC_TimebaseCalc(0); 			// use default reference clock
	init.prescale = ADC_PrescaleCalc(16000000, 0);	// 16MHz
	init.ovsRateSel = adcOvsRateSel2; 				// oversampling 2-4096
	init.warmUpMode = adcWarmupKeepInSlowAcq;		// used for high impedence, low freqency readings.

	/* Set scan data valid level to trigger */
	//ADC0->SCANCTRLX |= (ADC_SCAN_DVL - 1) << _ADC_SCANCTRLX_DVL_SHIFT;

	// configure the ADC
	ADC_Init(ADC0, &init);
}

void read_ADC0_scan(uint32_t* result_array[NUMBER_OF_SCANS]){
	uint8_t count = 0;

	// start the scan conversion
	ADC_Start(ADC0, adcStartScan);

	/* Wait while conversion is active */
	while (ADC0->STATUS & ADC_STATUS_SCANACT){
		if(ADC0->STATUS & ADC_STATUS_SCANDV){ // when the conversion is done
			*result_array[count++] = ADC_DataScanGet(ADC0);
		}
	}
}

uint32_t read_ADC0(){

	// Start the conversion
	ADC_Start(ADC0, adcStartSingle);

	/* Wait while conversion is active */
	while (ADC0->STATUS & ADC_STATUS_SINGLEACT){
		;
	}

	// check if the data is valid DOES NOT WORK FOR SOME REASON
	//while (~(ADC0->STATUS & ADC_STATUS_SINGLEDV)){
	//	;
	//}


	//DelayUs(1500); // wait 1.5ms


	/* Get ADC result */
	return ADC_DataSingleGet(ADC0);
}

const uint8_t* read_ADC0_Into_Message(){
	char result[4] = {0,0,0,0}; // 4 byte for 12 bit int

	// read the adc and save as string
	sprintf(result, "%" PRIu32, readADC0());

	// copy the result in to the message
	message[10] = result[0];
	message[11] = result[1];
	message[12] = result[2];
	message[13] = result[3];

	// return the string
	return message;
}
