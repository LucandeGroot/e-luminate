/*
 * E_Luminate.h
 *
 *  Created on: 13 dec. 2017
 *      Author: lucan
 */

#ifndef E_LUMINATE_H_
#define E_LUMINATE_H_

#include <stdint.h>


// 32768 ticks is one second, so #sec * 32768 == time
#define LED_TIMER_LENGTH 1*32768
#define LED_TIMER_CONTINUOUS 1 // Continuous constantly

// timer id's
enum TIMER_ID {
	LED_TIMER_ID = 0
};

// status codes for the lights
enum STATUS_CODES {
	STATUS_SUCCES = 0,
	STATUS_ERROR = 1
};

// function to check all lights
// returns STATUS_CODE enum
uint8_t check_lights();

// function to enable the LED timer
void start_LED_timer();

// function to disable the LED timer
void stop_LED_timer();

// toggle the output light green
void toggle_LED_green();

// set the LED green
void set_LED_green();

// set the LED red
void set_LED_red();


#endif /* E_LUMINATE_H_ */
