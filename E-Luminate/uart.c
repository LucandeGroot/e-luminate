/*
 * uart.c
 *
 *  Created on: 13 dec. 2017
 *      Author: lucan
 */
#include "uart.h"

#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"

// transmit a string
void transmit_string(uint8_t* string_to_send, uint8_t length){
	while(length--){
		transmit_char(string_to_send++);
	}
}

// function to send a char
void transmit_char(uint8_t* char_to_send){
	USART_Tx(USART0, *char_to_send);
}

void init_uart(){
	// Set the TX pin to push-pull output with pull-up
	GPIO_PinModeSet(TX_PORT, TX_PIN, gpioModePushPull, 1);


	// start the uart clock
	CMU_ClockEnable(cmuClock_USART0, true);

	/* Default config:
	 *
	 * Enable after init
	 * Current reference clock
	 * 115200 baud
	 * 16x oversampling
	 * 8 data bits
	 * No parity
	 * 1 stop bit
	 */
	USART_InitAsync_TypeDef uartinit = USART_INITASYNC_DEFAULT;

	// Make sure no flowcontrol
	uartinit.hwFlowControl = false;

	// send the configuration
	USART_InitAsync(USART0, &uartinit);

	// set the correct pin input (PD15)
	// see Table 6.7 Alternate functionality overview in the efr32bg1-datasheet (Page 139)
	// https://www.silabs.com/documents/login/data-sheets/efr32bg1-datasheet.pdf
	USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK)) | USART_ROUTELOC0_TXLOC_LOC23;

	// enable the TX pen
	USART0->ROUTEPEN = USART0->ROUTEPEN | USART_ROUTEPEN_TXPEN;
}

