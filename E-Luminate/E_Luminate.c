/*
 * E_Luminate.c
 *
 *  Created on: 13 dec. 2017
 *      Author: lucan
 */

#include "E_Luminate.h"
#include "adc.h"


uint8_t check_lights(){
	enum STATUS_CODES result = STATUS_SUCCES;


	// start the timer to toggle the LED
	start_LED_timer();

	// check the VDD voltage
	// start the ADC scan
	read_ADC0_scan();

	// check the car voltage (May not be connected)

	// check the MUX 1-8 power

	// stop the timer and set the LED to the correct color
	stop_LED_timer();

	// set the LED to represent the result
	if(result){ // everything is OK and return
		set_LED_green();
		return result;
	} else { // ERROR in light check, so set the led red and return
		set_LED_red();
		return result;
	}
}

void start_LED_timer(){
	// start a timer with the led timer ID
	gecko_cmd_hardware_set_soft_timer(LED_TIMER_LENGTH, LED_TIMER_ID, LED_TIMER_CONTINUOUS);
}

void stop_LED_timer(){
	// if the timer length is 0, the timer with the ID stops
	gecko_cmd_hardware_set_soft_timer(0, LED_TIMER_ID, LED_TIMER_CONTINUOUS);
}

void toggle_LED_green(){
	;// TODO
}

void set_LED_red(){
	;//TODO
}

void set_LED_green(){
	;//TODO
}
