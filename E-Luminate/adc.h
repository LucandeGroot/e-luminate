/*
 * adc.h
 *
 *  Created on: 5 dec. 2017
 *      Author: lucan
 */

#ifndef ADC_H_
#define ADC_H_

#include <stdint.h> // for uint

// timer interval 32798 is the # tick for one second
#define ADC_TIMER_INTERVAL 0.01*32768 // 10 ms (100Hz)

// the timer ID's
#define ADC_TIMER_ID 1


// functions prototypes
void initADC();
uint32_t readADC0();
const uint8_t* read_ADC0_Into_Message();

#endif /* ADC_H_ */
