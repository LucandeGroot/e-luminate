/*
 * uart.h
 *
 *  Created on: 13 dec. 2017
 *      Author: lucan
 */

#ifndef UART_H_
#define UART_H_

#include <stdint.h>

#define TX_PORT gpioPortD
#define TX_PIN	15

void init_uart();
void transmit_char(uint8_t*);
void transmit_string(uint8_t*, uint8_t);

#endif /* UART_H_ */
