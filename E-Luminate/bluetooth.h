/*
 * bluetooth.h
 *
 *  Created on: 5 dec. 2017
 *      Author: lucan
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include <stdint.h>

#define MAX_MTU_SIZE 14 // size of the message string
#define MAX_ADVERTISEMENT_PER_READING 1 // max advertisement per reading ==> reading refreshed every # advertisements
#define ADC_ADVERTISEMENT_INTERVAL 16	// 16*0,625 = 10 ms => 100 Hz
									  	// 80*0,625 = 50 ms => 20 Hz
/*
 * The message string
 * 0x02, 0x01, 0x06, 0x07, 0x09 => No idea why it's needed, but it won't send the string without it
 * 'E', 'P', 'A', 'R', 'K' => Giving the E-Connect an indication what the data is
 * 'T','E','M','P' => location of the data you want to send
 */
extern uint8_t message[]; // = {0x02, 0x01, 0x06, 0x07, 0x09, 'E', 'P', 'A', 'R', 'K', '1','2','3','4'}

void init_bluetooth();

#endif /* BLUETOOTH_H_ */
