/*
 * bluetooth.c
 *
 *  Created on: 5 dec. 2017
 *      Author: lucan
 */

#include "bluetooth.h"

#include "native_gecko.h"
#include "adc.h" // for read_adc0_into_message();
/*
 * The message string
 * 0x02, 0x01, 0x06, 0x07, 0x09 => No idea why it's needed, but it won't send the string without it
 * 'E', 'P', 'A', 'R', 'K' => Giving the E-Connect an indication what the data is
 * 'T','E','M','P' => location of the data you want to send
 */
uint8_t message[] = {0x02, 0x01, 0x06, 0x07, 0x09, 'E', 'P', 'A', 'R', 'K', '1','2','3','4'};

void init_bluetooth(){
	// send one advertisement
	gecko_cmd_le_gap_set_adv_timeout(MAX_ADVERTISEMENT_PER_READING);

	/* set the advertisement data
	 *  0 = advertisement packets
	 *  MAX_MTU_SIZE gives the lengt of the string returned by readADC0_string()
	 *  readADC0_string() returns a string that can be used for the advertisements
	 */
	gecko_cmd_le_gap_set_adv_data(0, MAX_MTU_SIZE, message);

	/* Set advertising parameters. 100ms advertisement interval. All channels used.
	 * The first two parameters are minimum and maximum advertising interval, both in
	 * units of 0.625ms, so parameter * 0,625 = #ms.
	 * The third parameter '7' sets advertising on all channels.
	 */
	gecko_cmd_le_gap_set_adv_parameters(ADC_ADVERTISEMENT_INTERVAL, ADC_ADVERTISEMENT_INTERVAL, 7);

	/* Start general advertising with user data (ADC reading) but unconnecteble */
	gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_non_connectable);
}
